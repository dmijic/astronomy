Meteor.startup(function () {

  let contact = new Contact();
  contact.push('phones', new Phone());
  contact.push('phones', new Phone());
  contact.push('phones', new Phone());
  contact.push('phones', new Phone());
  contact.push('phones', new Phone());

  console.log('_____________________');
  console.log('Number of phones: ' + contact.phones.length);
  console.log('_____________________');

  console.log('removing 4 phones');
  contact.pop('phones', 1);
  contact.pop('phones', -1);
  contact.pop('phones', 1);
  contact.pop('phones', -1);

  console.log('_____________________');
  console.log('Number of phones: ' + contact.phones.length);


});